macOS
=====

.. note::
    We're working on improved packaging for macOS, but for now the best way to
    install Idem on macOS is to follow the Python Pip installation guide.


.. grid:: 1

    .. grid-item-card:: Python Pip
        :link: /quickstart/install/pythonpip
        :link-type: doc

        Install with Python Pip

        :bdg-info:`Python`
