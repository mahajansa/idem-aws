import copy
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False,
    "localstack does not support requesting a certificate with EMAIL validation method",
)
@pytest.mark.asyncio
async def test_certificate_validation_with_email(
    hub, ctx, aws_certificate_manager_email
):
    name = aws_certificate_manager_email.get("name")
    resource_id = aws_certificate_manager_email.get("resource_id")
    ret_val = await hub.states.aws.acm.certificate_validation.present(
        ctx,
        name=name,
        resource_id=resource_id,
        certificate_arn=resource_id,
    )
    assert not ret_val["result"] and ret_val["comment"]
    assert (
        "aws.acm.certificate_validation is only valid for DNS validation"
        in ret_val["comment"][0]
    )


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_certificate_validation(
    hub,
    ctx,
    aws_certificate_manager_fqdns,
    aws_certificate_manager_import,
):
    # aws_certificate_manager_fqdns: Requested 'edu' Certificate
    name = aws_certificate_manager_fqdns.get("name")
    resource_id = aws_certificate_manager_fqdns.get("resource_id")
    timeout = {"describe": {"delay": 10, "max_attempts": 3}}

    validation_record_fqdns = []
    describe_ret = await hub.states.aws.acm.certificate_manager.describe(ctx)
    described_resource = describe_ret.get(resource_id).get(
        "aws.acm.certificate_manager.present"
    )
    details = dict(ChainMap(*described_resource))

    if details.get("domain_validation_options_validation"):
        for option in details.get("domain_validation_options_validation"):
            if option.get("resource_record"):
                if option.get("resource_record").get("Name"):
                    validation_record_fqdns.append(
                        option.get("resource_record").get("Name")
                    )

    assert 2 == len(validation_record_fqdns)

    # Passing incomplete or incorrect validation_record_fqdns
    ret = await hub.states.aws.acm.certificate_validation.present(
        ctx,
        name=name,
        resource_id=resource_id,
        certificate_arn=resource_id,
        validation_record_fqdns=validation_record_fqdns[0:1],
    )
    assert not ret["result"] and ret["comment"]
    assert "missing DNS validation record" in str(ret["comment"])

    # Passing obtained validation_record_fqdns
    # Checking the pending validation state
    ret = await hub.states.aws.acm.certificate_validation.present(
        ctx,
        name=name,
        resource_id=resource_id,
        certificate_arn=resource_id,
        validation_record_fqdns=validation_record_fqdns,
        timeout=timeout,
    )
    assert ret["old_state"] and ret["new_state"]
    assert ret["result"] and ret["comment"]
    assert f"PENDING_VALIDATION" in str(ret["comment"])

    # validation with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.acm.certificate_validation.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        certificate_arn=resource_id,
        validation_record_fqdns=validation_record_fqdns,
        timeout=timeout,
    )
    assert ret["result"] and ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert f"PENDING_VALIDATION" in str(ret["comment"])

    # aws_certificate_manager_import: Imported certificate is not "AMAZON" issued
    name = aws_certificate_manager_import.get("name")
    resource_id = aws_certificate_manager_import.get("resource_id")
    ret = await hub.states.aws.acm.certificate_validation.present(
        ctx,
        name=name,
        resource_id=resource_id,
        certificate_arn=resource_id,
        validation_record_fqdns=None,
    )
    assert ret["old_state"]
    assert not ret["result"] and ret["comment"]
    assert (
        f"Certificate '{resource_id}' is not Amazon issued, no validation necessary"
        in str(ret["comment"])
    )

    ret = await hub.states.aws.acm.certificate_validation.absent(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
