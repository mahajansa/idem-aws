import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update_security_groups(
    hub, ctx, aws_ec2_instance, aws_ec2_security_group_in_default_vpc, __test
):
    """
    The instance created with the fixture will have the default security group and the default subnet.
    The instance's subnet and security group should belong to the same network, so we update the security groups
    of the instance with another security group (created with a fixture) in the default VPC.
    """
    state = copy.copy(aws_ec2_instance)

    state["security_group_ids"] = [aws_ec2_security_group_in_default_vpc["resource_id"]]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"][
            "security_group_ids"
        ], "Instance's security groups should have been updated"
        assert (
            len(ret["changes"]["new"]["security_group_ids"]) == 1
        ), "Instance should have only one security group"
        assert (
            ret["changes"]["new"]["security_group_ids"][0]
            == aws_ec2_security_group_in_default_vpc["resource_id"]
        ), "Instance is not updated with the right security group"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_reset_security_groups(hub, ctx, aws_ec2_instance, __test):
    """
    Remove the security group added in the other test and add the default one again.
    """
    state = copy.copy(aws_ec2_instance)

    # Run the present state with a modified value, it should update the security group back to the default one
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert (
            ret["changes"]["new"]["security_group_ids"]
            == aws_ec2_instance["security_group_ids"]
        ), "Instance's security groups should have been updated"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
