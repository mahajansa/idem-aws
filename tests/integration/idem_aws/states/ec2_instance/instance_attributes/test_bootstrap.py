import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_create_no_auto(hub, ctx, aws_ec2_instance, __test):
    return
    state = copy.copy(aws_ec2_instance)
    state["bootstrap"] = [
        {
            "heist_manager": "salt",
            "auto_configure": False,
        }
    ]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        # No changes this time because autoconfigure wasn't set and the vpc isn't ready
        assert ret["changes"]["old"]["bootstrap"] == []
        assert ret["changes"]["new"]["bootstrap"] == []
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_create_auto(hub, ctx, aws_ec2_instance, __test):
    return
    state = copy.copy(aws_ec2_instance)
    state["bootstrap"] = [
        {
            "heist_manager": "salt",
            "auto_configure": True,
        }
    ]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["old"]["bootstrap"] == []
        assert ret["changes"]["new"]["bootstrap"] == ["salt.minion"]
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx, aws_ec2_instance, __test):
    return
    state = copy.copy(aws_ec2_instance)
    # Remove the changed tag from previous tests so that it gets deleted
    state["bootstrap"] = []

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before/during creation, report the changes made
    if __test <= 1:
        assert ret["changes"]["old"]["bootstrap"] == ["salt.minion"]
        assert ret["changes"]["new"]["bootstrap"] == []
    # After creation, no changes made with/without test
    else:
        assert not ret["changes"] is None
