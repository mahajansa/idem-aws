import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization_unit(hub, ctx, aws_organization):
    name = "idem-test-organization-unit-" + str(uuid.uuid4())

    roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    root_id = None

    if roots_resp:
        root_id = roots_resp["ret"]["Roots"][0]["Id"]

    create_tag = {"org": "idem", "env": "test"}

    # create test context
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # create organization unit in test
    create_ret = await hub.states.aws.organizations.organization_unit.present(
        test_ctx, name=name, parent_id=root_id, tags=create_tag
    )

    assert create_ret["result"], create_ret["comment"]
    assert (
        f"Would create aws.organizations.organization_unit '{name}'."
        in create_ret["comment"]
    )
    assert not create_ret.get("old_state")

    assert create_ret.get("new_state")
    new_state = create_ret.get("new_state")
    assert root_id == new_state["parent_id"]
    assert name == new_state["name"]
    assert (
        new_state["tags"] == create_tag
    ), "tags used in the create should match new state"

    # create organization unit in real
    create_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx, name=name, parent_id=root_id, tags=create_tag
    )

    assert create_ret["result"], create_ret["comment"]
    assert not create_ret.get("old_state")

    assert create_ret.get("new_state")
    assert create_ret.get("new_state")["resource_id"]

    created_state = create_ret.get("new_state")
    created_org_id = created_state["resource_id"]
    assert root_id == created_state["parent_id"]
    assert name == created_state["name"]
    assert (
        created_state["tags"] == create_tag
    ), "tags used in the create should match new state"

    # update organization unit

    updated_name = "idem-test-organization-unit-" + str(uuid.uuid4())
    update_tag = {"org": "idem-aws", "state": "temp"}

    # update organization unit in test
    update_ret = await hub.states.aws.organizations.organization_unit.present(
        test_ctx,
        name=updated_name,
        parent_id=root_id,
        tags=update_tag,
        resource_id=created_org_id,
    )

    assert update_ret["result"], update_ret["comment"]
    assert (
        f"Would update aws.organizations.organization_unit '{updated_name}'"
        in update_ret["comment"]
    )
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")
    updated_state = update_ret.get("new_state")
    assert (
        updated_state["tags"] == update_tag
    ), "tags used in the update should match updated state"
    assert (
        updated_state["name"] == updated_name
    ), "returned ou name should get updated ou name"
    assert (
        updated_state["resource_id"] == created_org_id
    ), "returned resource_id should match the state's identifier"

    old_state = update_ret.get("old_state")

    assert (
        old_state["tags"] == created_state["tags"]
    ), "tags used in the update should match updated state"
    assert (
        updated_state["name"] == updated_name
    ), "returned ou name should get updated ou name"
    assert (
        old_state["resource_id"] == created_state["resource_id"]
    ), "returned resource_id should match the state's identifier"

    # Update in real
    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=updated_name,
        parent_id=root_id,
        tags=update_tag,
        resource_id=created_org_id,
    )
    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")
    updated_state = update_ret.get("new_state")
    assert (
        updated_state["tags"] == update_tag
    ), "tags used in the update should match updated state"
    assert (
        updated_state["name"] == updated_name
    ), "returned ou name should get updated ou name"
    assert (
        updated_state["resource_id"] == created_org_id
    ), "returned resource_id should match the state's identifier"

    old_state = update_ret.get("old_state")

    assert (
        old_state["tags"] == created_state["tags"]
    ), "tags used in the update should match updated state"
    assert (
        updated_state["name"] == updated_name
    ), "returned ou name should get updated ou name"
    assert (
        old_state["resource_id"] == created_state["resource_id"]
    ), "returned resource_id should match the state's identifier"

    # describe organizational unit

    describe_ret = await hub.states.aws.organizations.organization_unit.describe(ctx)

    assert updated_state["name"] in describe_ret
    describe_params = describe_ret[updated_state["name"]].get(
        "aws.organizations.organization_unit.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert updated_state["name"] == described_resource_map["name"]
    assert root_id == described_resource_map["parent_id"]
    assert update_tag == described_resource_map["tags"]
    # delete organizational unit

    # Delete organizational unit with test flag
    ret = await hub.states.aws.organizations.organization_unit.absent(
        test_ctx, name=updated_state["name"], resource_id=updated_state["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.organizations.organization_unit '{updated_name}'"
        in ret["comment"]
    )

    # Delete organizational unit in real
    delete_ret = await hub.states.aws.organizations.organization_unit.absent(
        ctx, name=updated_state["name"], resource_id=updated_state["resource_id"]
    )

    if not hub.tool.utils.is_running_localstack(ctx):
        assert not delete_ret.get("new_state"), delete_ret.get("old_state")

        delete_ret = await hub.states.aws.organizations.organization_unit.absent(
            ctx, name=updated_state["name"], resource_id=updated_state["resource_id"]
        )
        assert not delete_ret.get("old_state") and not delete_ret.get("new_state")

        assert (
            f"aws.organizations.organization_unit '{updated_state['name']}' already absent"
            in delete_ret["comment"]
        )


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization_unit_update(
    hub, ctx, aws_organization, aws_organization_unit
):
    describe_ret = await hub.states.aws.organizations.organization_unit.describe(ctx)

    ou = describe_ret.get(aws_organization_unit["name"]).get(
        "aws.organizations.organization_unit.present"
    )
    described_resource_map = dict(ChainMap(*ou))
    "idem-test-organization-unit-random-" + str(uuid.uuid4())
    # create test context
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Update in test

    update_ret = await hub.states.aws.organizations.organization_unit.present(
        test_ctx,
        name=aws_organization_unit["name"],
        parent_id=described_resource_map["parent_id"],
        tags=aws_organization_unit["tags"],
        resource_id=aws_organization_unit["resource_id"],
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")

    assert (
        update_ret["new_state"] == aws_organization_unit
    ), "Can't have two OU with same name,hence no update"

    updated_name = "idem-test-ou-" + str(uuid.uuid4())
    update_tag = [
        {"Key": "org", "Value": "idem-aws"},
        {"Key": "env", "Value": "sandbox"},
    ]

    update_ret = await hub.states.aws.organizations.organization_unit.present(
        test_ctx,
        name=updated_name,
        parent_id=described_resource_map["parent_id"],
        tags=update_tag,
        resource_id=described_resource_map["resource_id"],
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")

    assert (
        update_ret["new_state"] != aws_organization_unit
    ), "state changes since name and tag was updated "

    # Update in real
    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=aws_organization_unit["name"],
        parent_id=described_resource_map["parent_id"],
        tags=aws_organization_unit["tags"],
        resource_id=aws_organization_unit["resource_id"],
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")
    assert (
        update_ret["new_state"] == aws_organization_unit
    ), "Can't have two OU with same name,hence no update"

    updated_name = "idem-test-ou-" + str(uuid.uuid4())
    update_tag = {"org": "idem-aws", "env": "sandbox"}

    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=updated_name,
        parent_id=described_resource_map["parent_id"],
        tags=update_tag,
        resource_id=described_resource_map["resource_id"],
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")

    assert (
        update_ret["new_state"] != aws_organization_unit
    ), "state changes since name and tag was updated "

    update_ret = await hub.states.aws.organizations.organization_unit.present(
        ctx,
        name=updated_name,
        parent_id=described_resource_map["parent_id"],
        resource_id=described_resource_map["resource_id"],
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state")

    assert update_ret.get("new_state")

    assert (
        f"aws.organizations.organization_unit '{updated_name}' already exists."
        in update_ret["comment"]
    )
