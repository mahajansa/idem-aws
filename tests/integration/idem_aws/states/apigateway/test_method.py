import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-resource-" + str(int(time.time())),
    "http_method": "GET",
    "authorization_type": "NONE",
    "api_key_required": False,
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_apigateway_rest_api, cleanup):
    global PARAMETER
    rest_api_id = aws_apigateway_rest_api.get("resource_id")

    resources = await hub.exec.boto3.client.apigateway.get_resources(
        ctx, restApiId=rest_api_id
    )
    parent_resource_id = resources["ret"]["items"][0].get("id")
    http_method = PARAMETER["http_method"]
    resource_id = f"{rest_api_id}-{parent_resource_id}-{http_method}"

    PARAMETER["parent_resource_id"] = parent_resource_id
    PARAMETER["rest_api_id"] = rest_api_id

    ctx["test"] = __test
    present_ret = await hub.states.aws.apigateway.method.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.apigateway.method", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource_id
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.apigateway.method", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["authorization_type"] == resource.get("authorization_type")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.method.describe(ctx)
    resource = describe_ret.get(PARAMETER["resource_id"]).get(
        "aws.apigateway.method.present"
    )
    resource_map = dict(ChainMap(*resource))
    assert PARAMETER["api_key_required"] == resource_map.get("api_key_required")
    assert PARAMETER["authorization_type"] == resource_map.get("authorization_type")
    assert PARAMETER["http_method"] == resource_map.get("http_method")
    assert PARAMETER["parent_resource_id"] == resource_map.get("parent_resource_id")
    assert PARAMETER["rest_api_id"] == resource_map.get("rest_api_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    updated_api_key_required = True
    updated_authorization_type = "AWS_IAM"
    new_parameter["api_key_required"] = updated_api_key_required
    new_parameter["authorization_type"] = updated_authorization_type

    ret = await hub.states.aws.apigateway.method.present(ctx, **new_parameter)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.method", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert new_parameter["name"] == resource["name"]
        assert new_parameter["resource_id"] == resource.get("resource_id")
        assert new_parameter["rest_api_id"] == resource.get("rest_api_id")
        assert new_parameter["parent_resource_id"] == resource.get("parent_resource_id")
        assert new_parameter["http_method"] == resource.get("http_method")
        assert new_parameter["authorization_type"] == resource.get("authorization_type")
        assert new_parameter["api_key_required"] == bool(
            resource.get("api_key_required")
        )

        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.apigateway.method", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )

    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent_method(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.method.absent(
        ctx,
        name=PARAMETER["name"],
        rest_api_id=PARAMETER["rest_api_id"],
        parent_resource_id=PARAMETER["parent_resource_id"],
        http_method=PARAMETER["http_method"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource["name"]
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["authorization_type"] == resource.get("authorization_type")
    assert PARAMETER["api_key_required"] == bool(resource.get("api_key_required"))
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.method",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.method",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.method.absent(
        ctx,
        name=PARAMETER["name"],
        rest_api_id=PARAMETER["rest_api_id"],
        parent_resource_id=PARAMETER["parent_resource_id"],
        http_method=PARAMETER["http_method"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.method",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigateway.method.absent(
            ctx,
            name=PARAMETER["name"],
            rest_api_id=PARAMETER["rest_api_id"],
            parent_resource_id=PARAMETER["parent_resource_id"],
            http_method=PARAMETER["http_method"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
