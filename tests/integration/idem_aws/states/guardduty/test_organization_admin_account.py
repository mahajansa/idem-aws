from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "enable_org_admin",
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup, aws_guardduty_detector):
    # create detector here so that it gets deleted after tests run
    aws_guardduty_detector.get("resource_id")
    global PARAMETER
    ctx["test"] = __test
    master_acc = await hub.exec.boto3.client.sts.get_caller_identity(ctx)
    account_id = master_acc["ret"]["Account"]
    PARAMETER["resource_id"] = account_id
    present_ret = await hub.states.aws.guardduty.organization_admin_account.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.guardduty.organization_admin_account",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.guardduty.organization_admin_account",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
        assert "ENABLED" == resource.get("admin_status")
    assert not present_ret["old_state"] and present_ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.guardduty.organization_admin_account.describe(
        ctx
    )
    resource_id = PARAMETER["resource_id"]
    resource_key = f"aws-guardduty-org_admin_account-{resource_id}"
    assert resource_key in describe_ret
    assert (
        "aws.guardduty.organization_admin_account.present" in describe_ret[resource_key]
    )
    described_resource = describe_ret[resource_key].get(
        "aws.guardduty.organization_admin_account.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")
    assert "ENABLED" == described_resource_map.get("admin_status")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.dependency(name="exec-list", depends=["present"])
async def test_exec_list(hub, ctx):
    ret = await hub.exec.aws.guardduty.organization_admin_account.list(
        ctx, name=PARAMETER["name"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert "ENABLED" == resource.get("admin_status")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.dependency(name="absent")
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    absent_ret = await hub.states.aws.guardduty.organization_admin_account.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret["old_state"] and not absent_ret["new_state"]
    resource = absent_ret["old_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.guardduty.organization_admin_account",
                name=PARAMETER["name"],
            )[0]
            in absent_ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.guardduty.organization_admin_account",
                name=PARAMETER["name"],
            )[0]
            in absent_ret["comment"]
        )
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["name"] == resource.get("name")
    assert "ENABLED" == resource.get("admin_status")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    absent_ret = await hub.states.aws.guardduty.organization_admin_account.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert (not absent_ret["old_state"]) and (not absent_ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.guardduty.organization_admin_account",
            name=PARAMETER["name"],
        )[0]
        in absent_ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_org_admin_account_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.aws.guardduty.organization_admin_account.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.guardduty.organization_admin_account",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.dependency(name="list-empty", depends=["absent"])
async def test_list_empty_resource(hub, ctx):
    ret = await hub.exec.aws.guardduty.organization_admin_account.list(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) == 0
    assert (
        hub.tool.aws.comment_utils.list_empty_comment(
            resource_type="aws.guardduty.organization_admin_account",
            name=PARAMETER["name"],
        )
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if not hub.tool.utils.is_running_localstack(ctx):
        if "resource_id" in PARAMETER:
            ret = await hub.states.aws.guardduty.organization_admin_account.absent(
                ctx,
                name=PARAMETER["name"],
                resource_id=PARAMETER["resource_id"],
            )
            assert ret["result"], ret["comment"]
