def test_auto_scaling_group_utils(hub):

    # When current state is empty and desired state is not empty. is_auto_scaling_updated should be true
    current_state = {}
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key", "Value": "value"}],
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert result

    # When current state is same as desired state and tags are not same. is_auto_scaling_updated should be false
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key", "Value": "value"}],
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert not result

    # When current state and desired state  are not same. is_auto_scaling_updated should be true
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    desired_state = {
        "name": "",
        "min_size": 3,
        "max_size": 5,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key", "Value": "value"}],
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert result

    # When desired state contains a new non-empty parameter. is_auto_scaling_updated should be true
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "launch_template": {"LaunchTemplateId": "lt-3"},
        "launch_configuration_name": "lc-2",
        "tags": [{"Key": "key1", "Value": "value1"}],
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert result

    # When vpc_zone_identifier in current state is not set and desired state is set  is_auto_scaling_updated should be true
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "vpc_zone_identifier": "lc-2",
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert result

    # When vpc_zone_identifier in current state is comma seperated and desired state is set without commas is_auto_scaling_updated should be true
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "vpc_zone_identifier": "vpc-1,vpc-2",
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "vpc_zone_identifier": "vpc-1",
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert result

    # When vpc_zone_identifier comma separated values in current state and desired state are same is_auto_scaling_updated should be false
    current_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "vpc_zone_identifier": "vpc-2,vpc-1",
    }
    desired_state = {
        "name": "",
        "min_size": 2,
        "max_size": 4,
        "vpc_zone_identifier": "vpc-1,vpc-2",
    }
    result = hub.tool.aws.autoscaling.auto_scaling_group_utils.is_update_required(
        current_state, desired_state
    )
    assert not result
