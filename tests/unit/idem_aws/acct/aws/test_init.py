import pytest
import yaml


async def test_gather(hub):
    """
    Verify that normal credentials are received
    """
    ACCT = """
    aws:
      default:
        aws_access_key_id: my_key_id
        aws_secret_access_key: my_secret_key
        region_name: us-west-1
        config:
            max_pool_connections: 50
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["aws_access_key_id"] == "my_key_id"
    assert ctx.acct["aws_secret_access_key"] == "my_secret_key"
    assert ctx.acct["region_name"] == "us-west-1"
    assert ctx.acct["aws_session_token"] is None
    assert ctx.acct["verify"] is None
    assert ctx.acct["use_ssl"] is True
    assert ctx.acct["config"] is not None
    assert ctx.acct["config"].max_pool_connections == 50


async def test_gather_defaults(hub):
    """
    Verify that all key values get populated in acct when unspecified
    """
    ACCT = """
    aws:
      default:
        null:
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["aws_access_key_id"] is None
    assert ctx.acct["aws_secret_access_key"] is None
    assert ctx.acct["region_name"] is None
    assert ctx.acct["aws_session_token"] is None
    assert ctx.acct["verify"] is None
    assert ctx.acct["use_ssl"] is True
    assert ctx.acct["config"]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    argnames="key", argvalues=["aws_access_key_id", "key_id", "id"]
)
async def test_gather_access_key_id(hub, key):
    ACCT = f"""
    aws:
      default:
        {key}: my_key_id
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["aws_access_key_id"] == "my_key_id"


@pytest.mark.asyncio
@pytest.mark.parametrize(
    argnames="key",
    argvalues=["aws_secret_access_key", "secret_access_key", "access_key", "key"],
)
async def test_gather_secret_access_key(hub, key):
    ACCT = f"""
    aws:
      default:
        {key}: my_secret_key
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["aws_secret_access_key"] == "my_secret_key"


@pytest.mark.asyncio
@pytest.mark.parametrize(
    argnames="key", argvalues=["aws_session_token", "session_token", "token", "key"]
)
async def test_gather_session_token(hub, key):
    ACCT = f"""
    aws:
      default:
        {key}: my_session_token
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["aws_session_token"] == "my_session_token"


@pytest.mark.asyncio
@pytest.mark.parametrize(argnames="value", argvalues=[True, False])
async def test_gather_use_ssl(hub, value):
    ACCT = f"""
    aws:
      default:
        use_ssl: {value}
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["use_ssl"] is value


@pytest.mark.asyncio
@pytest.mark.parametrize(argnames="value", argvalues=[True, False])
async def test_gather_verify(hub, value):
    ACCT = f"""
    aws:
      default:
        verify: {value}
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["verify"] is value


@pytest.mark.asyncio
@pytest.mark.parametrize(argnames="key", argvalues=["endpoint", "endpoint_url"])
async def test_gather_endpoint_url(hub, key):
    ACCT = f"""
    aws:
      default:
        {key}: my_endpoint:8080
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["endpoint_url"] == "my_endpoint:8080"


@pytest.mark.asyncio
async def test_gather_config(hub):
    ACCT = f"""
    aws:
      default:
        config:
          key: value
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))
    ctx = await hub.idem.acct.ctx("states.aws.", "default", acct_data=PROFILES)

    assert ctx.acct["config"] == {"key": "value"}
